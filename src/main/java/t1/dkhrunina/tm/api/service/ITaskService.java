package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name,
                       @Nullable String description);

}
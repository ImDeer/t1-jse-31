package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll().stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}